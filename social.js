define([
	'require',
	'madjoh_modules/ajax/ajax',
	'madjoh_modules/profile/profile',
	'madjoh_modules/styling/styling'
],
function(require, AJAX, Profile, Styling){
	var Social = {
		init : function(settings){
			hello.init(
				settings,
				{redirect_uri : 'https://adodson.com/hello.js/redirect.html'}
			);
		},
		login : function(provider, scope){
			var hi = hello(provider);
			return hi.login(scope);
		},

		Facebook : {
			register : function(){

			}
		},

		Twitter : {
			register : function(){
				return new Promise(function(resolve, reject){
					Social.Twitter.auth().then(function(){
						Profile.get(function(profile){
							if(profile.twitter_id) return resolve();

							hello('twitter').api('/me').then(function(result){
								Social.Twitter.login(result.id_str, null).then(function(){
									profile.twitter_id = result.id_str;
									Profile.save(profile, resolve);
								}, reject);
							}, reject);
						});
					}, reject);
				});
			},
			auth : function(){
				var session = hello('twitter').getAuthResponse();
				var currentTime = new Date().getTime() / 1000;
				if(session && session.access_token && session.expires > currentTime) return Promise.resolve();
				return Social.login('twitter');
			},
			login : function(twitter_id, token){
				var parameters = {
					twitter_id 	: twitter_id,
					token 		: token,
				};
				return AJAX.post('/user/login/twitter', {parameters : parameters});
			},
			getFriends : function(startIndex){
				return new Promise(function(resolve, reject){
					var url = startIndex || 'me/friends';
					hello('twitter').api(url, {limit: 35}).then(function(result){
						var twitterUsers = result.data;

						var twitterIds = [];
						for(var i = 0; i < twitterUsers.length; i++) twitterIds.push(twitterUsers[i].id_str);
						
						Social.Twitter.getScoredFriends(twitterIds).then(function(justdareUsers){
							var users = [];
							for(var i = 0; i < twitterUsers.length; i++){
								var user = null;
								for(var j = justdareUsers.length - 1; j >= 0; j--){
									if(justdareUsers[j].twitter_id === twitterUsers[i].id_str){
										user = justdareUsers[j];
										justdareUsers.splice(j, 1);
										break;
									}
								}

								if(user){
									user.page_index = result.paging.next;
									user.lastname 	+= ' (@' + twitterUsers[i].screen_name + ')'; // We show @twitter_username to understand that he comes from twitter
								}else{
									user = {
										id 					: i,
										twitterid 			: twitterUsers[i].id_str,
										firstname 			: twitterUsers[i].first_name,
										lastname 			: twitterUsers[i].last_name + ' (@' + twitterUsers[i].screen_name + ')',
										screenname 			: twitterUsers[i].screen_name,
										social_thumb 		: twitterUsers[i].thumbnail,
										friendship_status 	: -1,
										nb_common 			: 0,
										page_index 			: result.paging.next
									};
								}

								users.push(user);
							}
							resolve(users);
						}, reject);
					}, reject);
				});
			},
			searchFriends : function(startIndex, prefix){
				if(!prefix || prefix.length < 3) return Promise.resolve([]);
				prefix = prefix.toLowerCase();

				return new Promise(function(resolve, reject){
					var url = startIndex || 'me/friends';
					hello('twitter').api(url, {limit: 80}).then(function(result){
						var twitterUsers = result.data;

						var twitterIds = [];
						for(var i = twitterUsers.length - 1; i >= 0; i--){
							if(twitterUsers[i].name.toLowerCase().indexOf(prefix) === 0 || twitterUsers[i].screen_name.toLowerCase().indexOf(prefix) === 0) twitterIds.push(twitterUsers[i].id_str);
							else twitterUsers.splice(i, 1);
						}
						
						Social.Twitter.getScoredFriends(twitterIds).then(function(justdareUsers){
							var users = [];
							for(var i = 0; i < twitterUsers.length; i++){
								var user = null;
								for(var j = justdareUsers.length - 1; j >= 0; j--){
									if(justdareUsers[j].twitter_id === twitterUsers[i].id_str){
										user = justdareUsers[j];
										justdareUsers.splice(j, 1);
										break;
									}
								}

								if(user){
									user.page_index = result.paging.next;
									user.lastname 	+= ' (@' + twitterUsers[i].screen_name + ')'; // We show @twitter_username to understand that he comes from twitter
								}else{
									user = {
										id 					: i,
										twitterid 			: twitterUsers[i].id_str,
										firstname 			: twitterUsers[i].first_name,
										lastname 			: twitterUsers[i].last_name + ' (@' + twitterUsers[i].screen_name + ')',
										screenname 			: twitterUsers[i].screen_name,
										social_thumb 		: twitterUsers[i].thumbnail,
										friendship_status 	: -1,
										nb_common 			: 0,
										page_index 			: result.paging.next
									};
								}

								users.push(user);
							}
							resolve(users);
						}, reject);
					}, reject);
				});
			},
			getScoredFriends : function(twitterIds){
				var parameters = {twitter_ids : JSON.stringify(twitterIds)};
				return AJAX.post('/friendship/get/twitter', {parameters : parameters}).then(function(result){return result.data.users;});
			}
		},

		Contacts : {
			register : function(phone_number){
				return new Promise(function(resolve, reject){
					Profile.get(function(profile){
						if(profile.phone_number) return resolve();

						Social.Contacts.auth(phone_number, profile).then(function(){
							profile.phone_number = phone_number;
							Promise.save(profile, resolve);
						}, reject)
					});
				});
			},
			auth : function(phone_number, profile){
				var phoneRegex = new RegExp('^\\+[0-9]*$');
				if(phone_number && phone_number.length > 0 && phoneRegex.test(phone_number)) return Social.Contacts.login(phone_number);

				return new Promise(function(resolve, reject){
					Social.Contacts.getSimInfo(function(result){
						if(result && result.phoneNumber) return Social.Contacts.login(phone_number).then(resolve, reject);

						Social.Contacts.getDeviceContacts().then(function(contacts){
							contacts = Social.Contacts.processDeviceContacts(contacts, 0, true);

							for(var i = 0; i < contacts.length; i++){
								if(contacts[i].mail === profile.mail) return Social.Contacts.login(contacts[i].phone_number).then(resolve, reject);
							}
							return reject();
						}, reject)
					});
				});
			},
			login : function(phone_number){
				if(!phone_number) return Promise.reject('UNGIVEN_PHONE_NUMBER');

				var parameters = {phone_number : phone_number};
				return AJAX.post('/friendship/register/phone', {parameters : parameters});
			},
			getSimInfo : function(callback){
				if(!window.cordova) return callback({});
				window.plugins.sim.getSimInfo(callback, callback);
			},
			searchFriends : function(startIndex, prefix){return Social.Contacts.getFriends(startIndex, prefix);},
			getFriends : function(startIndex, prefix){
				return new Promise(function(resolve, reject){
					if(!startIndex) startIndex = -1;
					else startIndex = Math.floor(parseFloat(startIndex) * 100000);
					startIndex++;

					Social.Contacts.getDeviceContacts(prefix).then(function(contacts){
						contacts = Social.Contacts.processDeviceContacts(contacts, startIndex);

						Social.Contacts.getScoredFriends(contacts).then(function(users){
							for(var j = contacts.length - 1; j >= 0; j--){
								var page_index = '' + (startIndex + j) * 0.00001;
									page_index = page_index.substr(0, 7);
								contacts[j].page_index = page_index;

								var found = false;
								for(var i = 0; i < users.length; i++){
									if(users[i] && (users[i].phone_number && users[i].phone_number === contacts[j].phone_number) || (users[i].mail && users[i].mail === contacts[j].mail)){
										for(var key in users[i]) contacts[j][key] = users[i][key];
										contacts[j].firstname 	= contacts[j].displayName;
										contacts[j].lastname 	= '';
										contacts[j].page_index 	= page_index;
										found = true;
										break;
									}
								}
								if(!found && !contacts[j].displayName) contacts.splice(j, 1);
							}

							return resolve(contacts);
						}, reject);
					}, reject);
				});
			},
			processDeviceContacts : function(contacts, startIndex, full){
				for(var i = contacts.length - 1; i >= 0; i--){
					if(contacts[i].phoneNumbers && contacts[i].phoneNumbers.length > 0){
						contacts[i].phone_number = contacts[i].phoneNumbers[0].value.replace(/\s+/g, ''); // remove spaces
					}
					if(contacts[i].emails && contacts[i].emails.length > 0){
						contacts[i].mail = contacts[i].emails[0].value;
					}
					if(contacts[i].photos && contacts[i].photos.length > 0){
						contacts[i].photo_url = contacts[i].photos[0].value;
					}
					if(!contacts[i].phone_number && !contacts[i].mail){
						contacts.splice(i, 1);
						continue;
					}

					delete contacts[i].phoneNumbers;
					delete contacts[i].emails;
					delete contacts[i].photos;
				}

				contacts = contacts.sort(function(contact2, contact1){
					if(contact2.displayName > contact1.displayName) return 1;
					if(contact2.displayName < contact1.displayName) return -1;
					return 0;
				});

				if(!full) contacts = contacts.slice(startIndex, startIndex + 10);

				return contacts;
			},
			getDeviceContacts : function(prefix){
				if(!window.cordova){
					var contacts = [{emails : [{value : 'jonathanhattab@hotmail.fr'}], displayName : 'John H'}, {emails : [{value : 'xyz@madjoh.com'}], displayName : 'Adrien Liucio'}, {phoneNumbers : [{value : '0713125012'}], displayName : 'X USER 1'}, {phoneNumbers : [{value:'0613125012'}], displayName : 'B USER 1'}, {phoneNumbers : [{value:'0713125012'}], displayName : 'R USER 1'},{phoneNumbers : [{value:'0613125012'}], displayName : 'V USER 1'}, {phoneNumbers : [{value:'0713125012'}], displayName : 'Test 2'},{phoneNumbers : [{value:'0613125012'}], displayName : 'Test 1'}, {phoneNumbers : [{value:'0713125012'}], displayName : 'Test 2'},{phoneNumbers : [{value:'0613125012'}], displayName : 'Test 1'}, {phoneNumbers : [{value:'0713125012'}], displayName : 'Test 2'},{phoneNumbers : [{value:'0613125012'}], displayName : 'Test 1'}, {phoneNumbers : [{value:'0713125012'}], displayName : 'Test 2'},{phoneNumbers : [{value:'0613125012'}], displayName : 'Test 1'}, {phoneNumbers : [{value:'0713125012'}], displayName : 'Test 2'},{phoneNumbers : [{value:'0613125012'}], displayName : 'Test 1'}, {phoneNumbers : [{value:'0713125012'}], displayName : 'Test 2'},{phoneNumbers : [{value:'0613125012'}], displayName : 'Test 1'}, {phoneNumbers : [{value:'0713125012'}], displayName : 'Test 2'}, {emails : [{value : 'jonathanhattab@hotmail.fr'}], displayName : 'John H'}, {phoneNumbers : [{value : '0613125012'}], displayName : 'Z USER 1'}, {phoneNumbers : [{value : '0713125012'}], displayName : 'X USER 1'}, {phoneNumbers : [{value:'0613125012'}], displayName : 'B USER 1'}, {phoneNumbers : [{value:'0713125012'}], displayName : 'R USER 1'},{phoneNumbers : [{value:'0613125012'}], displayName : 'V USER 1'}, {phoneNumbers : [{value:'0713125012'}], displayName : 'Test 2'},{phoneNumbers : [{value:'0613125012'}], displayName : 'Test 1'}, {phoneNumbers : [{value:'0713125012'}], displayName : 'Test 2'},{phoneNumbers : [{value:'0613125012'}], displayName : 'Test 1'}, {phoneNumbers : [{value:'0713125012'}], displayName : 'Test 2'},{phoneNumbers : [{value:'0613125012'}], displayName : 'Test 1'}, {phoneNumbers : [{value:'0713125012'}], displayName : 'Test 2'},{phoneNumbers : [{value:'0613125012'}], displayName : 'Test 1'}, {phoneNumbers : [{value:'0713125012'}], displayName : 'Test 2'},{phoneNumbers : [{value:'0613125012'}], displayName : 'Test 1'}, {phoneNumbers : [{value:'0713125012'}], displayName : 'Test 2'},{phoneNumbers : [{value:'0613125012'}], displayName : 'Test 1'}, {phoneNumbers : [{value:'0713125012'}], displayName : 'Test 2'}];
					if(prefix){
						prefix = prefix.toLowerCase();
						for(var i = contacts.length - 1; i >= 0; i--){
							if(contacts[i].displayName.toLowerCase().indexOf(prefix) !== 0) contacts.splice(i, 1);
						}
					}
					return Promise.resolve(contacts);
				}

				return new Promise(function(resolve, reject){
					var fields = [navigator.contacts.fieldType.displayName, navigator.contacts.fieldType.phoneNumbers, navigator.contacts.fieldType.photos, navigator.contacts.fieldType.addresses, navigator.contacts.fieldType.emails];
					var options = new ContactFindOptions();
						options.multiple 		= true;
						options.hasPhoneNumber 	= true;
						options.desiredFields 	= fields;

					if(Styling.isAndroid()) fields.push(navigator.contacts.fieldType.name);

					if(prefix) options.filter = prefix;

					navigator.contacts.find([navigator.contacts.fieldType.name],
						function(contacts){
							for(var i = contacts.length - 1; i >= 0; i--){
								for(var key in contacts[i]){
									if(fields.indexOf(key) === -1) delete contacts[i][key];
								}
							}

							return resolve(contacts);
						},
						reject,
						options
					);
				});
			},
			getScoredFriends : function(contacts){
				var contact_creds = [];
				for(var i = 0; i < contacts.length; i++){
					contact_creds.push({phone_number : contacts[i].phone_number, mail : contacts[i].mail});
				}

				var parameters = {contact_creds : JSON.stringify(contact_creds)};
				return AJAX.post('/friendship/get/contact', {parameters : parameters}).then(function(result){return result.data.users;});
			}
		},

		Google : {
			register : function(){
				return new Promise(function(resolve, reject){
					Social.Google.auth().then(function(){
						Profile.get(function(profile){
							if(profile.google_id) return resolve();

							hello('google').api('/me').then(function(result){
								Social.Google.login(result.etag, null).then(function(){
									profile.google_id = result.etag;
									Profile.save(profile, resolve);
								}, reject);
							}, reject);
						});
					}, reject);
				});
			},
			auth : function(){
				var session = hello('google').getAuthResponse();
				var currentTime = new Date().getTime() / 1000;
				if(session && session.access_token && session.expires > currentTime) return Promise.resolve();
				return Social.login('google');
			},
			login : function(google_id, token){
				var parameters = {
					google_id 	: google_id,
					token 		: token
				};
				return AJAX.post('/user/login/google', {parameters : parameters});
			},
			getFriends : function(startIndex){
				return new Promise(function(resolve, reject){
					var url = startIndex || 'me/contacts';

					hello('google').api(url, {limit: 35}).then(function(result, error){
						if(error) return reject(error);
						resolve(result);
					}, reject);
				});
			},
			getScoredFriends : function(twitterIds){
				
			}
		}
	};

	return Social;
});
